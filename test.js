var log = {
    info: function(info) {//function內的info為輸入的參數
        console.log('Info:' + info);
    },
    warning: function(warning) {
        console.log('Warning:' + warning);
    },
    error: function(error) {
        console.log('Error:' + error);
    }
};
module.exports=log //模組(程式)要給別的模組(程式)使用，必須加這個！！